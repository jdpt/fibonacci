# Procesos agiles de desarrollo
# Serie de Fibonacci
# Juan David Poveda
# Cod. 201522970
# jd.poveda11@uniandes.edu.co

def calcularSerie(n):
   if n == 1:
      return 1
   elif n == 0:   
      return 0            
   else:                      
      return calcularSerie(n-1) + calcularSerie(n-2)

def pedirNumero():
    try:
        iteraciones = int(input("Ingrese el numero de iteraciones: "))

        if iteraciones <= 0:
            print("Se debe ingresar un numero valido (positivo)")
            pedirNumero()
        else:
            print("Serie de Fibonacci:")
            for i in range(iteraciones):
                print calcularSerie(i),
                print "  ",

    except NameError:
        print("Se debe ingresar un numero valido (positivo)")
        pedirNumero()

pedirNumero()


